# TestbeamBatchScripts
A set of basic scripts to submit batch jobs for ITk testbeam reconstruction on slurm based systems.



batch.sh is the template for the basic slurm job, and go_batch.sh modifies this script to create a submission script for each individual run. Although currently these scripts run with Radek's strip reconstruction repo (https://gitlab.cern.ch/rprivara/strip-reco), they can be modified to work with a range of reconstructions. 

## Getting started 
1. Edit lines 7-12  of batch.sh to work with your slurm system - eg. partition name, email, etc. 
2. Edit lines 23-25 of batch.sh to setup your environment for you reconstruction (including setting up any modules needed)
3. If needed, modify line 28 of batch.sh if you are using a different reconstruction repository
4. Modify lines 6-8 in go_batch.sh to match the runs you are trying to reconstruction. Note: since each run is submitted individually, to make the jobs most efficient, it's reccomended you first submit the alignment run first - usually the run at 50DAC. This can be done by setting run_st, run_en and run_al all to this run number. Once this has finished (or if you see mask.conf and prealign.conf in the geometry folder for that stream), you can then submit the rest of the runs and these steps won't be repeated in each batch job.
5. **HIGHLY RECOMENDED*** Before submitting all jobs with ```source go_batch.sh```, it is suggested you leave line 16 commented out - this way all the scripts will be created and you can inspect them to make sure there aren't any typos/mistakes that would cause them to fail and impact your priority on your system. Once you are confident, you can uncomment this line and again ```source go_batch.sh```
