#!/bin/bash

###Modify the following slurm commands to work with your slurm system, 
###as well as your email if you would like to be notified if a job fails. 
###You can also increase/decrease the time and memory allocation for each job  ###

#SBATCH --partition=cascade
#SBATCH --job-name="26Feb24-RUN_N"
#SBATCH --mem=5120
#SBATCH --mail-user=myemail@cern.ch
#SBATCH --mail-type=FAIL
#SBATCH --time=03:30:00

# check that the script is launched with sbatch
if [ "x$SLURM_JOB_ID" == "x" ]; then
   echo "You need to submit your job to the queuing system with sbatch"
   exit 1
fi


module purge 
###Modify the following lines to setup environment and navigate to your reconstruction repository ###
source /data/projects/punim0011/bellac/qual/setup.sh
cd /data/projects/punim0011/bellac/qual/strip-reco/
source setup_bella.sh 

###This is the main command that is run ###
bash run.sh RUN_N RUN_N RUN_A

##Job monitor command to list the resource usage
my-job-stats -a -n -s
