Version=$(date "+%d%m%Y_%H%M")
mkdir $Version
cd $Version


run_st=3028
run_en=3042
run_al=3027

for ((i=${run_st}; i<=${run_en}; i++)); do
    cat ../batch.sh  | sed s/RUN_N/${i}/g | sed s/RUN_A/${run_al}/g > submit_batch_${i}.sh
    ###The following line is only commented out for testing purposes - when uncommented it will submit 
    ###each job from run_st to run_en as an individual job. It is recomended you test first so your 
    ###priority in the system isn't affected
    
    #sbatch submit_batch_${i}.sh
done 
